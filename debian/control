Source: pyside
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Build-Depends:
 cmake (>= 2.8.4+dfsg.1-3~),
 debhelper-compat (= 9),
 dh-python,
 dpkg-dev (>= 1.15.6),
 libgl1-mesa-dri <!nocheck>,
 libphonon-dev,
 libqt4-dev (>= 4:4.7.0),
 libqt4-opengl-dev (>= 4:4.7.0),
 libqt4-sql-sqlite (>= 4:4.7.0) <!nocheck>,
 libqtwebkit-dev,
 libshiboken-dev (>= 1.2.2),
 phonon,
 phonon-backend-gstreamer <!nocheck>,
 python-all-dev (>= 2.6.6-3),
 python-setuptools,
 python3-all-dev,
 rename,
 shiboken (>= 1.2.2),
 xauth <!nocheck>,
 xvfb <!nocheck>,
Build-Conflicts:
 phonon-backend-null,
Standards-Version: 3.9.5
Vcs-Git: https://salsa.debian.org/python-team/packages/pyside.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/pyside
Homepage: http://www.pyside.org/

Package: python-pyside
Architecture: all
Depends:
 python-pyside.phonon (>= ${binary:Version}),
 python-pyside.qtcore (>= ${binary:Version}),
 python-pyside.qtdeclarative (>= ${binary:Version}),
 python-pyside.qtgui (>= ${binary:Version}),
 python-pyside.qthelp (>= ${binary:Version}),
 python-pyside.qtnetwork (>= ${binary:Version}),
 python-pyside.qtopengl (>= ${binary:Version}),
 python-pyside.qtscript (>= ${binary:Version}),
 python-pyside.qtsql (>= ${binary:Version}),
 python-pyside.qtsvg (>= ${binary:Version}),
 python-pyside.qttest (>= ${binary:Version}),
 python-pyside.qtuitools (>= ${binary:Version}),
 python-pyside.qtwebkit (>= ${binary:Version}),
 python-pyside.qtxml (>= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Description: Python bindings for Qt4 (big metapackage)
 Qt is a cross-platform C++ application framework. Qt's primary feature
 is its rich set of widgets that provide standard GUI functionality.
 .
 Python bindings for Qt4 framework. This is a metapackage for all modules.

Package: python3-pyside
Architecture: all
Depends:
 python3-pyside.phonon (>= ${binary:Version}),
 python3-pyside.qtcore (>= ${binary:Version}),
 python3-pyside.qtdeclarative (>= ${binary:Version}),
 python3-pyside.qtgui (>= ${binary:Version}),
 python3-pyside.qthelp (>= ${binary:Version}),
 python3-pyside.qtnetwork (>= ${binary:Version}),
 python3-pyside.qtopengl (>= ${binary:Version}),
 python3-pyside.qtscript (>= ${binary:Version}),
 python3-pyside.qtsql (>= ${binary:Version}),
 python3-pyside.qtsvg (>= ${binary:Version}),
 python3-pyside.qttest (>= ${binary:Version}),
 python3-pyside.qtuitools (>= ${binary:Version}),
 python3-pyside.qtwebkit (>= ${binary:Version}),
 python3-pyside.qtxml (>= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Description: Python3 bindings for Qt4 (big metapackage)
 Qt is a cross-platform C++ application framework. Qt's primary feature
 is its rich set of widgets that provide standard GUI functionality.
 .
 Python3 bindings for Qt4 framework. This is a metapackage for all modules.

Package: libpyside1.2
Architecture: any
Depends:
 ${misc:Depends},
 ${python:Depends},
 ${shlibs:Depends},
Pre-Depends:
 ${misc:Pre-Depends},
Multi-Arch: same
Description: Python bindings for Qt 4 (base files)
 Qt is a cross-platform C++ application framework. Qt's primary feature
 is its rich set of widgets that provide standard GUI functionality.
 .
 This package contains base files used by all modules.

Package: libpyside-py3-1.2
Architecture: any
Depends:
 ${misc:Depends},
 ${python3:Depends},
 ${shlibs:Depends},
Pre-Depends:
 ${misc:Pre-Depends},
Multi-Arch: same
Description: Python3 bindings for Qt 4 (base files)
 Qt is a cross-platform C++ application framework. Qt's primary feature
 is its rich set of widgets that provide standard GUI functionality.
 .
 This package contains base files used by all python3 modules.

Package: libpyside-dev
Architecture: any
Section: libdevel
Depends:
 libpyside-py3-1.2 (= ${binary:Version}),
 libpyside1.2 (= ${binary:Version}),
 libqt4-dev (>= 4:4.7.0),
 ${misc:Depends},
 ${shlibs:Depends},
Replaces:
 libpyside0.3 (<< 0.4.0),
Breaks:
 libpyside0.3 (<< 0.4.0),
Description: Python bindings for Qt 4 (development files)
 Qt is a cross-platform C++ application framework. Qt's primary feature
 is its rich set of widgets that provide standard GUI functionality.
 .
 This package provides the development headers for the libpyside library.

Package: python-pyside.qtcore
Architecture: any
Depends:
 libpyside1.2 (= ${binary:Version}),
 ${misc:Depends},
 ${pycmakedeps:Depends},
 ${python:Depends},
 ${shlibs:Depends},
Description: Qt 4 core module - Python bindings
 Qt is a cross-platform C++ application framework. Qt's primary feature
 is its rich set of widgets that provide standard GUI functionality.
 .
 This package provides Python bindings for the QtCore module.

Package: python3-pyside.qtcore
Architecture: any
Depends:
 libpyside-py3-1.2 (= ${binary:Version}),
 ${misc:Depends},
 ${pycmakedeps:Depends},
 ${python3:Depends},
 ${shlibs:Depends},
Description: Qt 4 core module - Python3 bindings
 Qt is a cross-platform C++ application framework. Qt's primary feature
 is its rich set of widgets that provide standard GUI functionality.
 .
 This package provides Python3 bindings for the QtCore module.

Package: python-pyside.qtdeclarative
Architecture: any
Depends:
 libpyside1.2 (= ${binary:Version}),
 ${misc:Depends},
 ${pycmakedeps:Depends},
 ${python:Depends},
 ${shlibs:Depends},
Description: Qt 4 Declarative module - Python bindings
 Qt is a cross-platform C++ application framework. Qt's primary feature
 is its rich set of widgets that provide standard GUI functionality.
 .
 This package provides Python bindings for the QtDeclarative module.

Package: python3-pyside.qtdeclarative
Architecture: any
Multi-Arch: same
Depends:
 libpyside-py3-1.2 (= ${binary:Version}),
 ${misc:Depends},
 ${pycmakedeps:Depends},
 ${python3:Depends},
 ${shlibs:Depends},
Description: Qt 4 Declarative module - Python3 bindings
 Qt is a cross-platform C++ application framework. Qt's primary feature
 is its rich set of widgets that provide standard GUI functionality.
 .
 This package provides Python3 bindings for the QtDeclarative module.

Package: python-pyside.qtgui
Architecture: any
Depends:
 libpyside1.2 (= ${binary:Version}),
 ${misc:Depends},
 ${pycmakedeps:Depends},
 ${python:Depends},
 ${shlibs:Depends},
Description: Qt 4 GUI module - Python bindings
 Qt is a cross-platform C++ application framework. Qt's primary feature
 is its rich set of widgets that provide standard GUI functionality.
 .
 This package provides Python bindings for the QtGui module.

Package: python3-pyside.qtgui
Architecture: any
Multi-Arch: same
Depends:
 libpyside-py3-1.2 (= ${binary:Version}),
 ${misc:Depends},
 ${pycmakedeps:Depends},
 ${python3:Depends},
 ${shlibs:Depends},
Description: Qt 4 GUI module - Python3 bindings
 Qt is a cross-platform C++ application framework. Qt's primary feature
 is its rich set of widgets that provide standard GUI functionality.
 .
 This package provides Python3 bindings for the QtGui module.

Package: python-pyside.qthelp
Architecture: any
Depends:
 libpyside1.2 (= ${binary:Version}),
 ${misc:Depends},
 ${pycmakedeps:Depends},
 ${python:Depends},
 ${shlibs:Depends},
Description: Qt 4 help module - Python bindings
 Qt is a cross-platform C++ application framework. Qt's primary feature
 is its rich set of widgets that provide standard GUI functionality.
 .
 This package provides Python bindings for the QtHelp module.

Package: python3-pyside.qthelp
Architecture: any
Multi-Arch: same
Depends:
 libpyside-py3-1.2 (= ${binary:Version}),
 ${misc:Depends},
 ${pycmakedeps:Depends},
 ${python3:Depends},
 ${shlibs:Depends},
Description: Qt 4 help module - Python3 bindings
 Qt is a cross-platform C++ application framework. Qt's primary feature
 is its rich set of widgets that provide standard GUI functionality.
 .
 This package provides Python3 bindings for the QtHelp module.

Package: python-pyside.qtnetwork
Architecture: any
Depends:
 libpyside1.2 (= ${binary:Version}),
 ${misc:Depends},
 ${pycmakedeps:Depends},
 ${python:Depends},
 ${shlibs:Depends},
Description: Qt 4 network module - Python bindings
 Qt is a cross-platform C++ application framework. Qt's primary feature
 is its rich set of widgets that provide standard GUI functionality.
 .
 This package provides Python bindings for the QtNetwork module.

Package: python3-pyside.qtnetwork
Architecture: any
Multi-Arch: same
Depends:
 libpyside-py3-1.2 (= ${binary:Version}),
 ${misc:Depends},
 ${pycmakedeps:Depends},
 ${python3:Depends},
 ${shlibs:Depends},
Description: Qt 4 network module - Python3 bindings
 Qt is a cross-platform C++ application framework. Qt's primary feature
 is its rich set of widgets that provide standard GUI functionality.
 .
 This package provides Python3 bindings for the QtNetwork module.

Package: python-pyside.qtopengl
Architecture: any
Depends:
 libpyside1.2 (= ${binary:Version}),
 ${misc:Depends},
 ${pycmakedeps:Depends},
 ${python:Depends},
 ${shlibs:Depends},
Description: Qt 4 OpenGL module - Python bindings
 Qt is a cross-platform C++ application framework. Qt's primary feature
 is its rich set of widgets that provide standard GUI functionality.
 .
 This package provides Python bindings for the QtOpenGL module.
 .
 OpenGL is a standard API for rendering 3D graphics. OpenGL only deals with 3D
 rendering and provides little or no support for GUI programming issues.

Package: python3-pyside.qtopengl
Architecture: any
Multi-Arch: same
Depends:
 libpyside-py3-1.2 (= ${binary:Version}),
 ${misc:Depends},
 ${pycmakedeps:Depends},
 ${python3:Depends},
 ${shlibs:Depends},
Description: Qt 4 OpenGL module - Python3 bindings
 Qt is a cross-platform C++ application framework. Qt's primary feature
 is its rich set of widgets that provide standard GUI functionality.
 .
 This package provides Python3 bindings for the QtOpenGL module.
 .
 OpenGL is a standard API for rendering 3D graphics. OpenGL only deals with 3D
 rendering and provides little or no support for GUI programming issues.

Package: python-pyside.phonon
Architecture: any
Depends:
 libpyside1.2 (= ${binary:Version}),
 ${misc:Depends},
 ${pycmakedeps:Depends},
 ${python:Depends},
 ${shlibs:Depends},
Description: Qt 4 Phonon module - Python bindings
 Qt is a cross-platform C++ application framework. Qt's primary feature
 is its rich set of widgets that provide standard GUI functionality.
 .
 This package provides Python bindings for the Phonon multimedia module.

Package: python3-pyside.phonon
Architecture: any
Multi-Arch: same
Depends:
 libpyside-py3-1.2 (= ${binary:Version}),
 ${misc:Depends},
 ${pycmakedeps:Depends},
 ${python3:Depends},
 ${shlibs:Depends},
Description: Qt 4 Phonon module - Python3 bindings
 Qt is a cross-platform C++ application framework. Qt's primary feature
 is its rich set of widgets that provide standard GUI functionality.
 .
 This package provides Python3 bindings for the Phonon multimedia module.

Package: python-pyside.qtscript
Architecture: any
Depends:
 libpyside1.2 (= ${binary:Version}),
 ${misc:Depends},
 ${pycmakedeps:Depends},
 ${python:Depends},
 ${shlibs:Depends},
Description: Qt 4 script module - Python bindings
 Qt is a cross-platform C++ application framework. Qt's primary feature
 is its rich set of widgets that provide standard GUI functionality.
 .
 This package provides Python bindings for the QtScript and QtScriptTools
 modules.

Package: python3-pyside.qtscript
Architecture: any
Multi-Arch: same
Depends:
 libpyside-py3-1.2 (= ${binary:Version}),
 ${misc:Depends},
 ${pycmakedeps:Depends},
 ${python3:Depends},
 ${shlibs:Depends},
Description: Qt 4 script module - Python3 bindings
 Qt is a cross-platform C++ application framework. Qt's primary feature
 is its rich set of widgets that provide standard GUI functionality.
 .
 This package provides Python3 bindings for the QtScript and QtScriptTools
 modules.

Package: python-pyside.qtsql
Architecture: any
Depends:
 libpyside1.2 (= ${binary:Version}),
 ${misc:Depends},
 ${pycmakedeps:Depends},
 ${python:Depends},
 ${shlibs:Depends},
Description: Qt 4 SQL module - Python bindings
 Qt is a cross-platform C++ application framework. Qt's primary feature
 is its rich set of widgets that provide standard GUI functionality.
 .
 This package provides Python bindings for the QtSql module.

Package: python3-pyside.qtsql
Architecture: any
Multi-Arch: same
Depends:
 libpyside-py3-1.2 (= ${binary:Version}),
 ${misc:Depends},
 ${pycmakedeps:Depends},
 ${python3:Depends},
 ${shlibs:Depends},
Description: Qt 4 SQL module - Python3 bindings
 Qt is a cross-platform C++ application framework. Qt's primary feature
 is its rich set of widgets that provide standard GUI functionality.
 .
 This package provides Python3 bindings for the QtSql module.

Package: python-pyside.qtsvg
Architecture: any
Depends:
 libpyside1.2 (= ${binary:Version}),
 ${misc:Depends},
 ${pycmakedeps:Depends},
 ${python:Depends},
 ${shlibs:Depends},
Description: Qt 4 SVG module - Python bindings
 Qt is a cross-platform C++ application framework. Qt's primary feature
 is its rich set of widgets that provide standard GUI functionality.
 .
 This package provides Python bindings for the QtSvg module.
 .
 Scalable Vector Graphics (SVG) is a language for describing two-dimensional
 graphics and graphical applications in XML.

Package: python3-pyside.qtsvg
Architecture: any
Multi-Arch: same
Depends:
 libpyside-py3-1.2 (= ${binary:Version}),
 ${misc:Depends},
 ${pycmakedeps:Depends},
 ${python3:Depends},
 ${shlibs:Depends},
Description: Qt 4 SVG module - Python3 bindings
 Qt is a cross-platform C++ application framework. Qt's primary feature
 is its rich set of widgets that provide standard GUI functionality.
 .
 This package provides Python3 bindings for the QtSvg module.
 .
 Scalable Vector Graphics (SVG) is a language for describing two-dimensional
 graphics and graphical applications in XML.

Package: python-pyside.qttest
Architecture: any
Depends:
 libpyside1.2 (= ${binary:Version}),
 ${misc:Depends},
 ${pycmakedeps:Depends},
 ${python:Depends},
 ${shlibs:Depends},
Description: Qt 4 test module - Python bindings
 Qt is a cross-platform C++ application framework. Qt's primary feature
 is its rich set of widgets that provide standard GUI functionality.
 .
 This package provides Python bindings for the QtTest module.

Package: python3-pyside.qttest
Architecture: any
Multi-Arch: same
Depends:
 libpyside-py3-1.2 (= ${binary:Version}),
 ${misc:Depends},
 ${pycmakedeps:Depends},
 ${python3:Depends},
 ${shlibs:Depends},
Description: Qt 4 test module - Python3 bindings
 Qt is a cross-platform C++ application framework. Qt's primary feature
 is its rich set of widgets that provide standard GUI functionality.
 .
 This package provides Python3 bindings for the QtTest module.

Package: python-pyside.qtuitools
Architecture: any
Depends:
 libpyside1.2 (= ${binary:Version}),
 ${misc:Depends},
 ${pycmakedeps:Depends},
 ${python:Depends},
 ${shlibs:Depends},
Description: Qt 4 UI tools module - Python bindings
 Qt is a cross-platform C++ application framework. Qt's primary feature
 is its rich set of widgets that provide standard GUI functionality.
 .
 This package provides Python bindings for the QtUiTools module.

Package: python3-pyside.qtuitools
Architecture: any
Multi-Arch: same
Depends:
 libpyside-py3-1.2 (= ${binary:Version}),
 ${misc:Depends},
 ${pycmakedeps:Depends},
 ${python3:Depends},
 ${shlibs:Depends},
Description: Qt 4 UI tools module - Python3 bindings
 Qt is a cross-platform C++ application framework. Qt's primary feature
 is its rich set of widgets that provide standard GUI functionality.
 .
 This package provides Python3 bindings for the QtUiTools module.

Package: python-pyside.qtwebkit
Architecture: any
Depends:
 libpyside1.2 (= ${binary:Version}),
 ${misc:Depends},
 ${pycmakedeps:Depends},
 ${python:Depends},
 ${shlibs:Depends},
Description: Qt 4 WebKit module - Python bindings
 Qt is a cross-platform C++ application framework. Qt's primary feature
 is its rich set of widgets that provide standard GUI functionality.
 .
 This package provides Python bindings for the QtWebKit module.

Package: python3-pyside.qtwebkit
Architecture: any
Multi-Arch: same
Depends:
 libpyside-py3-1.2 (= ${binary:Version}),
 ${misc:Depends},
 ${pycmakedeps:Depends},
 ${python3:Depends},
 ${shlibs:Depends},
Description: Qt 4 WebKit module - Python3 bindings
 Qt is a cross-platform C++ application framework. Qt's primary feature
 is its rich set of widgets that provide standard GUI functionality.
 .
 This package provides Python3 bindings for the QtWebKit module.

Package: python-pyside.qtxml
Architecture: any
Depends:
 libpyside1.2 (= ${binary:Version}),
 ${misc:Depends},
 ${pycmakedeps:Depends},
 ${python:Depends},
 ${shlibs:Depends},
Description: Qt 4 XML module - Python bindings
 Qt is a cross-platform C++ application framework. Qt's primary feature
 is its rich set of widgets that provide standard GUI functionality.
 .
 This package provides Python bindings for the QtXml and QtXmlPatterns modules.

Package: python3-pyside.qtxml
Architecture: any
Multi-Arch: same
Depends:
 libpyside-py3-1.2 (= ${binary:Version}),
 ${misc:Depends},
 ${pycmakedeps:Depends},
 ${python3:Depends},
 ${shlibs:Depends},
Description: Qt 4 XML module - Python3 bindings
 Qt is a cross-platform C++ application framework. Qt's primary feature
 is its rich set of widgets that provide standard GUI functionality.
 .
 This package provides Python3 bindings for the QtXml and QtXmlPatterns modules.
